/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var pluginsDownloadCmd = &cobra.Command{
	Use:   "download",
	Short: "Download dependencies",
	Long:  `Download dependencies`,
	PreRun: func(cmd *cobra.Command, args []string) {
		if err := loadWorkspace(); err != nil {
			log.Fatal(err)
		}
	},
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 1 {
			plugin := workspace.Blocks[args[0]]
			err := plugin.Download(args[0])
			CheckErr(err)
		} else {
			for p := range workspace.Blocks {
				log.Info("Attempting to download plugin ", p)

				plugin := workspace.Blocks[p]
				err := plugin.Download(p)
				CheckErr(err)
			}
		}
	},
}

func init() {
	blocksCmd.AddCommand(pluginsDownloadCmd)
}
