/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	goErrors "errors"
	"strings"

	"github.com/go-playground/validator/v10"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var stackCmd = &cobra.Command{
	Use:   "stack",
	Short: "Show stack",
	Long:  `Show stack`,
	Run: func(cmd *cobra.Command, args []string) {
		showWorkspace()
	},
}

func init() {
	rootCmd.AddCommand(stackCmd)
}

type ImageConfig struct {
	Reference string `mapstructure:"reference" json:"reference" validate:"required"`
	Version   string `mapstructure:"version" json:"version" validate:"required"`
}

type Workspace struct {
	Name      string                 `mapstructure:"name" json:"name" validate:"required"`
	Display   string                 `mapstructure:"display,omitempty" json:"display,omitempty"`
	Image     ImageConfig            `mapstructure:"image" json:"image" validate:"required"`
	Labels    []string               `mapstructure:"labels,omitempty" json:"labels,omitempty"`
	Config    map[string]interface{} `mapstructure:"config,omitempty,remain" json:"config,omitempty"`
	Blocks    map[string]Block       `mapstructure:"blocks" json:"blocks" validate:"dive,required"`
	Workflows map[string]Workflow    `mapstructure:"workflows,omitempty" json:"workflows,omitempty"`
}

func (c *Workspace) Validate() error {
	validate := validator.New()

	err := validate.Struct(c)

	if err != nil {

		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			log.Error(err)
			return nil
		}

		for _, err := range err.(validator.ValidationErrors) {
			log.Error("Configuration option `" + strings.ToLower(err.Namespace()) + "` failed to validate: " + err.Tag())
		}

		// from here you can create your own error messages in whatever language you wish
		return goErrors.New("Error validating Workspace")
	}
	return nil
}
