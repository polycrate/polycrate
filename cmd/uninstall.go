/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// installCmd represents the install command.
var uninstallCmd = &cobra.Command{
	Use:   "uninstall",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	//Args: cobra.MinimumNArgs(1),
	// PersistentPreRun: func(cmd *cobra.Command, args []string) {
	// 	stackDir = filepath.Join(viper.GetString("acsdir"), cmd.Flag("stack").Value.String())
	// 	viper.Set("stackdir", stackDir)
	// 	initConfig()
	// },
	Run: func(cmd *cobra.Command, args []string) {
		log.Info("Installing Cloud Stack")
		log.Info("Using Kubeconfig at ", viper.GetString("kubeconfig"))
		loadWorkspace()
		if len(args) == 1 {
			uninstallComponent(args[0])
		} else {
			uninstallComponents()
		}
	},
}

func init() {
	//rootCmd.AddCommand(uninstallCmd)
}

func uninstallComponent(component string) {

	componentCommands := map[string][]string{
		"iaas":    {"ansible-playbook", "-v", "uninstall-iaas.yml"},
		"kaas":    {"ansible-playbook", "-i", "$CLOUDSTACK_CONTEXT/inventory.yml", "uninstall-kaas.yml"},
		"paas":    {"ansible-playbook", "-v", "uninstall-paas.yml"},
		"bastion": {"ansible-playbook", "-i", "$CLOUDSTACK_CONTEXT/inventory.yml", "-v", "uninstall-bastion.yml"},
	}

	componentCommand := componentCommands[component]

	if viper.GetString("command") != "" {
		componentCommand = strings.Split(viper.GetString("command"), ",")
		log.Debug("Executing custom command: " + strings.Join(componentCommand, " "))
	}

	env := GetContainerEnv()

	mounts := []string{
		strings.Join([]string{workspaceDir, "/workspace"}, ":"),
		strings.Join([]string{kubeconfig, "/root/.kube/config"}, ":"),
		strings.Join([]string{workspaceConfigFile, "/tmp/Stackfile"}, ":"),
	}

	if devDir != "" {
		mounts = append(mounts, strings.Join([]string{devDir, "/cloudstack"}, ":"))
	}

	ports := []string{}

	_, err := RunContainer(
		imageRef,
		imageVersion,
		componentCommand,
		env,
		mounts,
		viper.GetBool("pull"), // Pull
		workdir,
		ports,
	)
	CheckErr(err)

	log.Info("Successfully installed ", component)
}

func uninstallComponents() {
	// https://lornajane.net/posts/2020/accessing-nested-config-with-viper
	var components = []string{"iaas"}

	allComponents := viper.Sub("components")

	log.WithFields(log.Fields{
		"stack": viper.GetString("stack.name"),
	}).Info("Starting component uninstallation")

	for _, component := range components {

		componentConfig := allComponents.Sub(component)
		enabled := componentConfig.GetBool("enabled")

		if enabled {
			// acs install component akp
			fmt.Println("Uninstalling " + component)
		}
	}
	log.WithFields(log.Fields{
		"stack": viper.GetString("stack.name"),
	}).Info("Finished component uninstallation")
}
