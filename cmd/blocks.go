/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bufio"
	goErrors "errors"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var pluginCallExitCode int

// installCmd represents the install command
var blocksCmd = &cobra.Command{
	Use:   "plugins",
	Short: "Control Cloudstack plugins",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

		// loadStatefile()
		//discoverKubernetesDistro()
		saveRuntimeStackfile()

		// TODO: verifyPlugin(plugin, pluginCommand)
		var err error
		pluginCallExitCode, err = callPlugin(block, action)
		CheckErr(err)
	},
	PreRun: func(cmd *cobra.Command, args []string) {
		if err := loadWorkspace(); err != nil {
			log.Fatal(err)
		}
		block = args[0]
		action = args[1]
	},
	// PersistentPreRun: func(cmd *cobra.Command, args []string) {
	// 	if err := loadStatefile(); err != nil {
	// 		log.Fatal(err)
	// 	}
	// 	addHistoryItem(cmd, "in progress")
	// },
	// PersistentPostRun: func(cmd *cobra.Command, args []string) {
	// 	updateHistoryItemStatus(strconv.Itoa(pluginCallExitCode))
	// 	writeHistory()
	// },
}

func init() {
	rootCmd.AddCommand(blocksCmd)
}

type GitConfig struct {
	Repository string `mapstructure:"repository" json:"repository" validate:"required_if=SourceConfig.Type git"`
	Path       string `mapstructure:"path" json:"path"`
	Branch     string `mapstructure:"branch" json:"branch"`
	Tag        string `mapstructure:"tag" json:"tag"`
}

type SourceConfig struct {
	Git  GitConfig `mapstructure:"git,omitempty" json:"git,omitempty"`
	Type string    `mapstructure:"type" json:"type" validate:"required_if=SourceConfig.Type SourceConfig"`
}

type Action struct {
	Display     string   `mapstructure:"display,omitempty" json:"display,omitempty"`
	Interactive bool     `mapstructure:"interactive,omitempty" json:"interactive,omitempty"`
	Labels      []string `mapstructure:"labels,omitempty" json:"labels,omitempty"`
	Script      []string `mapstructure:"script,omitempty" json:"script,omitempty" validate:"required_if=Command"`
}

type Block struct {
	Actions  map[string]Action      `mapstructure:"actions,omitempty" json:"actions,omitempty"`
	Clone    string                 `mapstructure:"clone,omitempty" json:"clone,omitempty"`
	Config   map[string]interface{} `mapstructure:"config,omitempty" json:"config,omitempty"`
	Labels   []string               `mapstructure:"labels,omitempty" json:"labels,omitempty"`
	Scope    string                 `mapstructure:"scope,omitempty" json:"scope,omitempty"`
	Source   SourceConfig           `mapstructure:"source,omitempty" json:"source,omitempty"`
	Template bool                   `mapstructure:"template,omitempty" json:"template,omitempty"`
	Version  string                 `mapstructure:"version" json:"version"`
}

func (c *Block) Download(pluginName string) error {
	pluginDir := filepath.Join(workspaceDir, "plugins", pluginName)
	_, err := os.Stat(pluginDir)
	if os.IsNotExist(err) || (!os.IsNotExist(err) && force) {

		if c.Source.Type != "" {
			if c.Source.Type == "git" {
				downloadDirName := strings.Join([]string{"cloudstack", "plugin", pluginName}, "-")
				downloadDir := filepath.Join("/tmp", downloadDirName)
				log.Debug("Downloading to " + downloadDir)

				_, err := os.Stat(downloadDir)
				if os.IsNotExist(err) {
					log.Debug("Temporary download directory does not exist. Cloning from git repository " + c.Source.Git.Repository)
					err := cloneRepository(c.Source.Git.Repository, downloadDir, c.Source.Git.Branch, c.Source.Git.Tag)
					if err != nil {
						return err
					}
				} else {
					log.Debug("Temporary download directory already exists")
				}

				// Remove .git directory
				if c.Source.Git.Path == "" || c.Source.Git.Path == "/" {
					pluginGitDirectory := filepath.Join(downloadDir, ".git")

					if _, err := os.Stat(pluginGitDirectory); !os.IsNotExist(err) {
						var args = []string{"-rf", pluginGitDirectory}
						log.Debug("Running command: rm ", strings.Join(args, " "))
						err = exec.Command("rm", args...).Run()
						//log.Debug("Removed .git directory at " + pluginGitDirectory)
						CheckErr(err)
					}
				}

				// Create plugin dir
				log.Debug("Attempting to create plugin directory at " + pluginDir)
				err = os.MkdirAll(pluginDir, os.ModePerm)
				if err != nil {
					return err
				}

				// Copy contents of dependency.path to /context/dependency.name
				copyArgs := []string{"-r", filepath.Join(downloadDir, c.Source.Git.Path) + "/.", pluginDir}
				log.Debug("Running command: cp ", strings.Join(copyArgs, " "))
				_, err = exec.Command("cp", copyArgs...).Output()
				if err != nil {
					return err
				}

				// Delete downloadDir
				err = os.RemoveAll(downloadDir)
				if err != nil {
					return err
				}

				log.Info("Successfully downloaded ", pluginName)
			}
		}
	} else {
		log.Warn("Plugin ", pluginName, " already exists. Use --force to download anyways. CAUTION: this will override your current plugin directory")
	}
	return nil
}

func (c *Action) GetExecutionScript() []string {
	// Prepare script
	scriptSlice := []string{
		"#!/bin/bash",
		"set -euo pipefail",
	}

	script := append(scriptSlice, c.Script...)
	return script
}

func (c *Action) ValidateScript() error {
	if c.Script == nil {
		return goErrors.New("No script found for command")
	}
	return nil
}

func (c *Action) SaveExecutionScript() (error, string) {
	f, err := ioutil.TempFile("/tmp", "cloudstack."+workspace.Name+".run.*.sh")
	if err != nil {
		return err, ""
	}
	datawriter := bufio.NewWriter(f)

	for _, data := range c.GetExecutionScript() {
		_, _ = datawriter.WriteString(data + "\n")
	}
	datawriter.Flush()
	log.Debug("Saved script to " + f.Name())

	err = os.Chmod(f.Name(), 0755)
	if err != nil {
		return err, ""
	}

	// Closing file descriptor
	// Getting fatal errors on windows WSL2 when accessing
	// the mounted script file from inside the container if
	// the file descriptor is still open
	// Works flawlessly with open file descriptor on M1 Mac though
	// It's probably safer to close the fd anyways
	f.Close()
	return nil, f.Name()
}
